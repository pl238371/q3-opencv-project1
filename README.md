import cv2
import numpy as np
input_image = cv2.imread('/Users/ethanfan/PycharmProjects/opencv circle detection/venv/test_easy.jpg', cv2.IMREAD_COLOR)
input_image2 = cv2.imread('/Users/ethanfan/PycharmProjects/opencv circle detection/venv/test_easy2.jpg', cv2.IMREAD_COLOR)
gray = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
gray_blurred = cv2.blur(gray, (3, 3))
detected_circles = cv2.HoughCircles(gray_blurred,
                                    cv2.HOUGH_GRADIENT, 1, 20, param1=80,
                                    param2=50, minRadius=30, maxRadius=50)
if detected_circles is not None:
    detected_circles = np.uint16(np.around(detected_circles))
    for pt in detected_circles[0, :]:
        a, b, r = pt[0], pt[1], pt[2]
        cv2.circle(input_image, (a, b), r, (0, 0, 255), 2)
        cv2.circle(input_image, (a, b), 1, (255, 0, 255), 3)
gray2 = cv2.cvtColor(input_image2, cv2.COLOR_BGR2GRAY)
gray_blurred2 = cv2.blur(gray2, (3, 3))
detected_circles2 = cv2.HoughCircles(gray_blurred2,
                                    cv2.HOUGH_GRADIENT, 1, 20, param1=80,
                                    param2=50, minRadius=50, maxRadius=100)
if detected_circles2 is not None:
    detected_circles2 = np.uint16(np.around(detected_circles2))
    for pt in detected_circles2[0, :]:
        a, b, r = pt[0], pt[1], pt[2]
        cv2.circle(input_image2, (a, b), r, (0, 0, 255), 2)
        cv2.circle(input_image2, (a, b), 1, (255, 0, 255), 3)
cv2.imshow("Circle", input_image)
cv2.imshow("Circle 2", input_image2)
cv2.waitKey(0)
                                                          
                                                                   
                                                                   
                                                                   
                                                                   
import cv2
import numpy as np
input_image = cv2.imread('/Users/local/PycharmProjects/Q3 OpenCV/test_l2.jpg')
rows, cols, ch = input_image.shape
cv2.circle(input_image, (210, 40), 5, (0, 0, 255), -1)
cv2.circle(input_image, (1219, 32), 5, (0, 0, 255), -1)
cv2.circle(input_image, (-178, 956), 5, (0, 0, 255), -1)
cv2.circle(input_image, (1770, 959), 5, (0, 0, 255), -1)
pts1 = np.float32([[210, 40], [1219, 32], [-178, 956], [1770, 959]])
pts2 = np.float32([[-47, 10], [920, 0], [80, 790], [1109, 905]])
matrix = cv2.getPerspectiveTransform(pts1, pts2)
result = cv2.warpPerspective(input_image, matrix, (cols, rows))
gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)
gray_blurred = cv2.blur(gray, (3, 3))
detected_circles = cv2.HoughCircles(gray_blurred,
                                    cv2.HOUGH_GRADIENT, 1, 20, param1=80,
                                    param2=50, minRadius=110, maxRadius=130)
if detected_circles is not None:
    detected_circles = np.uint16(np.around(detected_circles))
    for pt in detected_circles[0, :]:
        a, b, r = pt[0], pt[1], pt[2]
        cv2.circle(result, (a, b), r, (0, 0, 255), 2)
        cv2.circle(result, (a, b), 1, (255, 0, 255), 3)
pts3 = np.float32([[-47, 10], [920, 0], [80, 790], [1109, 905]])
pts4 = np.float32([[210, 40], [1219, 32], [-178, 956], [1770, 959]])
matrix2 = cv2.getPerspectiveTransform(pts3, pts4)
result2 = cv2.warpPerspective(result, matrix2, (cols, rows))
cv2.imshow("Circle", result)
cv2.imshow("Perspective transformation", result2)
cv2.waitKey(0)
cv2.destroyAllWindows()